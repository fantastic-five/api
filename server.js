import express from "express";
import mongoose from "mongoose";

import mainRouter from "./routes/mainRouter";

const app = express();
const db = mongoose.connect(
  "mongodb://fantastic5:fantastic-5@ds219100.mlab.com:19100/shamy-amy"
);
const port = process.env.PORT || 5656;

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

app.use("/api", mainRouter);

app.listen(port, () => {
  console.log(`http://localhost:${port}`);
});
