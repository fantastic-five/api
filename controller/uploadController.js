import IssueModel from '../models/IssueModel';
import ExifImage from 'exif';
import uuidv1 from 'uuid/v1';
import gpsUtil from 'gps-util';
import stream from 'stream';
import fs from 'fs';
import streamifier from 'streamifier';

import azure from 'azure-storage';
import path from 'path';
import sharp from 'sharp';
import * as constants from './constants';
import request from 'request';


const blobService = azure.createBlobService();
const container = 'imagecontainer';

function getImageExifData(req, res) {
    try {
        console.log("File size: " + req.file.size);
        console.log("File original name: " + req.file.originalname);
        console.log("Req body location: " + req.body.location);
        new ExifImage({
            image: req.file.buffer
        }, function(error, exifData) {
            if (error) {
                console.log('Error: ' + error.message);
                res.sendStatus(500);
            } else {
                console.log(exifData); // Do something with your data!
                let gpsLocation = exifData.gps;
                if (!gpsLocation.GPSLatitude && req.body.location) {
                    console.log("No location in exifdata,reading body.location: " + req.body.location);
                    gpsLocation = { ...JSON.parse(req.body.location),
                        manualLocation: true
                    }
                    console.log(gpsLocation);
                }
                if (!gpsLocation.GPSLatitude && !gpsLocation.latitude) {
                    res.status(500).send('No gps location');
                }
                getIssueFromImage(req.file.originalname, req.file.buffer, req.file.size, gpsLocation, (err, item, thumbnailName) => {
                    if (err) {
                        res.status(500).send(err);
                        return;
                    }
                    item.save(err => {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            res.status(201).send(item);
                            createAndUploadThumbnail(item.id, container, thumbnailName, req.file.buffer);
                        }
                    });
                });

            }
        });
    } catch (error) {
        console.log('Error: ' + error.message);
        res.sendStatus(500);
    }
}

function getIssueFromImage(imageName, imageBuffer, size, gpsLocation, callback) {
    let id = uuidv1();
    uploadToBlob(id, imageName, imageBuffer, size, (err, url, thumbnailName) => {
        if (err) {
            return callback(err);
        }

        console.log("Uploaded to: " + url);
        getCategory(url, (err, response) => {
            if (err) {
                console.log('Error from image detection: ' + err);
                return callback(err);
            }
            console.log('Response from image detection: ' + JSON.stringify(response));
            let detectionResponse = JSON.parse(response.body);
            let confidence = detectionResponse.confidence;
            let category = detectionResponse.result;
            console.log(`Found ${category} with confidence ${confidence}`);
            let item = new IssueModel({
                "id": id,
                "img": url,
                "coords": {
                    "lat": gpsLocation.manualLocation ? gpsLocation.latitude : gpsUtil.toDD(...gpsLocation.GPSLatitude),
                    "long": gpsLocation.manualLocation ? gpsLocation.longitude : gpsUtil.toDD(...gpsLocation.GPSLongitude)
                },
                "category": checkThreshold(category, confidence) ? category : 'none',
                "confidence": confidence,
                "description": ''
            })

            return callback(null, item, thumbnailName);
        });

    });
}

function checkThreshold(category, confidence) {
    if (constants.categories.indexOf(category) >= 0) {
        let threshold = constants.thresholds[category];
        console.log('Threshold for ' + category+': '+ threshold);
        if (confidence > threshold) {
            return true;
        }
    }
    return false;
}

function getCategory(imageUrl, callback) {
    var clientServerOptions = {
        uri: constants.imageDetectionUrl,
        body: JSON.stringify({
            url: imageUrl
        }),
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    request(clientServerOptions, function(error, response) {
        return callback(error, response);
    });
}

function getRandomCategory() {
    let textArray = [
        'ambrosia',
        'trash',
        'pothole',
        'none'
    ];
    let randomNumber = Math.floor(Math.random() * textArray.length);
    return textArray[randomNumber];
}

function createAndUploadThumbnail(id, container, thumbnailName, inputBuffer) {
    sharp(inputBuffer)
        .rotate()
        .resize(200)
        .toBuffer()
        .then(data => {
            let filesize = data.length;
            console.log('thumbnail size: ' + filesize);
            let dataStream = streamifier.createReadStream(data);
            blobService.createBlockBlobFromStream(container, thumbnailName, dataStream, filesize, function(error, result) {
                if (!error) {
                    let url = blobService.getUrl(container, thumbnailName);
                    console.log(`${thumbnailName} successfully uploaded to Azure Blob Storage. URL:`)
                    console.log(url);
                    IssueModel.update({
                        id: id
                    }, {
                        thumbnailImg: url
                    }, (err, numberAffected) => {
                        if (err) {
                            console.log('error updating item');
                        } else {
                            console.log(`Updated ${numberAffected} items`);

                        }
                    });
                } else {
                    console.error("here:" + error);
                }
            });
        })
        .catch(err => {
            console.log(err);
        });
}

function uploadToBlob(id, fileName, fileBuffer, size, callback) {
    let extName = path.extname(fileName),
        imageId = uuidv1(),
        blobName = imageId + extName,
        thumbnailName = imageId + '_thumbnail' + extName;
    console.log(extName);
    console.log(blobName);
    let readableStream = streamifier.createReadStream(fileBuffer);
    console.log(size);
    blobService.createContainerIfNotExists(container, {
        publicAccessLevel: 'blob'
    }, (error, result, response) => {
        blobService.createBlockBlobFromStream(container, blobName, readableStream, size, function(error, result) {
            if (!error) {
                let id = blobService.getUrl(container, blobName);
                console.log(`${blobName} successfully uploaded to Azure Blob Storage. URL:`)
                console.log(id);
                return callback(null, id, thumbnailName);
            }
            console.error("here:" + error);
            return callback(error);
        });
    });
}

export {
    getImageExifData
}