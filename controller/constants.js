const categories = ['ambrosia', 'trash', 'pothole'];
const imageDetectionUrl = 'http://hack2018.cloudapp.net/api/v1/queries';

const thresholds = {
    ambrosia: 0.65,
    trash: 0.60,
    pothole: 0.60
}
export {
    categories, imageDetectionUrl, thresholds
};