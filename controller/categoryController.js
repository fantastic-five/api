import IssueModel from '../models/IssueModel';
import uuidv1 from 'uuid/v1';
import sendNotification, { sendMail } from './notificationController'

import util from 'util';

function saveIssueCategory(req, res) {
    console.log('save issue: ' + JSON.stringify(req.body));
    let id = req.body.id;
    let cat = req.body.category;
    let desc = req.body.description

    var query = {
        id: id
    };
    var newData = {};
    if (desc) {
        newData.description = desc;
    }
    if (cat) {
        newData.category = cat;
    }

    var issue = IssueModel.update(query, newData, function(err, doc) {
        if (err) {
            console.log('update error');
            return res.send(500, {
                error: err
            });
        } else {
            console.log('update success: ' + JSON.stringify(doc));
            sendMail(id, (error) => {
                return res.sendStatus(200);
            });
        }
    });
}


export {
    saveIssueCategory
}