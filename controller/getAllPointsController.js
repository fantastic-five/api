import IssueModel from '../models/IssueModel';
import * as constants from './constants';

function getAllPoints(req, res) {
    console.log('get all points');

    IssueModel.find({ category: { $ne: "none" } }, (err, items) => {
        let response = {
            categories: constants.categories,
            reports: items
        }
        res.json(response);
    }) 
}

export {
    getAllPoints
}