import IssueModel from '../models/IssueModel';
import uuidv1 from 'uuid/v1';
import stream from 'stream';
import fs from 'fs';
import streamifier from 'streamifier';

import azure from 'azure-storage';
import path from 'path';
import sgMail from '@sendgrid/mail';

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

function sendNotification(req, res) {
    console.log('send notification');
    let id = req.body.id;
    console.log('issue id: ' + id);

    sendMail(id, (error) => {
        if (error) {
            res.send(500, {
                error: error
            });
        } else {
            res.sendStatus(200);
        }
    });
}


function sendMail(id, callback) {
    let issue = IssueModel.findOne({
        id: id
    }, function(err, doc) {
        if (err) {
            return callback(err);
        }
        let issueCategory = doc.category;
        if (issueCategory == 'pothole') {
            issueCategory = 'potholes'
        }

        let msg = `Hi,\n I am Amy and my friends reported ${issueCategory} at the following GPS coordinates: ${doc.coords.lat}, ${doc.coords.long}. Please take the appropriate actions!\n Thanks,\nAmy`
        let mapHtml = `<a href="https://www.google.com/maps/?q=${doc.coords.lat},${doc.coords.long}">position on the map </a>`;
        let imageHtml = '<a href="' + doc.img + '">Click here to see a picture of the issue. </a>';
        let msgHTML = `Hi,<br/><br/> I am Amy and my friends reported ${issueCategory} at the following ${mapHtml}: ${doc.coords.lat}, ${doc.coords.long}. <br/><br/>${imageHtml}<br/><br/> I would appreciate if you took the appropriate actions! <br/><br/> Thanks and all the best,<br/>Amy`

        const email = {
            to: process.env.SENDGRID_TO_EMAILS,
            from: process.env.SENGRID_FROM_EMAIL,
            subject: "Shamy Amy found an issue in your city",
            text: msg,
            html: msgHTML
        };

        sgMail.send(email)
            .then(() => {
                console.log("email sent ok");
                IssueModel.update({
                    id: id
                }, {
                    emailSent: true,
                    emailSentTimestamp: new Date()
                }, function(err, doc) {
                    if (err) {
                        return callback(err);
                    }
                    return callback(null);
                })
            }).catch(error => {
                console.log("Error sending email");
                console.error(error);
                return callback(error);
            });
    });

}
export {
    sendNotification,
    sendMail
}