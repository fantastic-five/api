import mongoose from "mongoose";

const Schema = mongoose.Schema;

const model = new Schema({
  id: String,
  img: String,
  thumbnailImg: String,
  description: String,
  coords: Object,
  category: String,
  confidence: Number,
  confirmed: Boolean,
  emailSent:Boolean,
  emailSentTimestamp: Date

});

export default mongoose.model("issue", model);
