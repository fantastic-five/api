import mongoose from "mongoose";

const Schema = mongoose.Schema;

const model = new Schema({
  order: {
    type: Number,
    get: v => Math.round(v),
    set: v => Math.round(v)
  },
  test: String
});

export default mongoose.model("route", model);
