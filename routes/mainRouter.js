import express from 'express';
import multer from 'multer';
import bodyParser from "body-parser";
import * as uploadController from '../controller/uploadController';
import * as getAllPointsController from '../controller/getAllPointsController';
import * as categoryController from '../controller/categoryController';
import * as notificationController from '../controller/notificationController';


const storage = multer.memoryStorage()
const upload = multer({ storage: storage })
const router = express.Router();



router.use(upload.single('photo')).post('/upload', uploadController.getImageExifData);
router.get('/getAllPoints', getAllPointsController.getAllPoints);
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));
router.use('/issue', bodyParser.json({type: 'application/json'}));
router.use(bodyParser.urlencoded({ extended: true }));
router.post('/issue', categoryController.saveIssueCategory);
router.use('/notification', bodyParser.json({type: 'application/json'}));
router.post('/notification', notificationController.sendNotification);

router.route('/')
    .get((req, res) => {
        Example.find({}, (err, items) => {
            res.json(items)
        })  
    })
    .post((req, res) => {
        let item = new Example(req.body);
        item.save(err => {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(201).send(item);
            }
        });
    });


router.use('/:id', (req, res, next)=>{
    Example.findById(req.params.id, (err, item)=>{
        if (err) {
            res.status(500).send(err);
        } else {
            req.item = item;
            next();
        }
    });
})

router.route('/:id')
    .get((req, res) => {
        res.json(req.item);
    })
    .put((req,res) => {
        if (req.body._id) {
            delete req.body._id;
        }

        Object.assign(item, req.body);
        item.save()
        res.json(item);
    })
    .delete((req,res)=>{
        req.item.remove(err => {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(204).send('removed');
            }
        });
    });
    

export default router;